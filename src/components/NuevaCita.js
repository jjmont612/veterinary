import React, { Component } from 'react';
import uuid from  'uuid';
import PropTypes from 'prop-types';

const stateInitial = {
    cita: {
      mascota: '',
      propietario: '',
      fecha: '',
      hora: '',
      sintomas: ''
    },
    error : false
}

class NuevaCita extends Component {
    state = { ...stateInitial }
    
    // cuando el usuario escribe en losinputs
    handleChage = e => {
    //   console.log(e.target.name + ': ' + e.target.value);

      //colocar lo que el usuario escribe en state
      this.setState({
        cita: {
          ...this.state.cita,
          [e.target.name] : e.target.value
        }
      })
    }

    // cuando el usuario envia el formulario
    handleSubmit = e => {
        e.preventDefault();
        // extraer los valores del state
        const { mascota, propietario, fecha, hora, sintomas } = this.state.cita;
        
        // validar que todos los campos esten llenos
        if( mascota === '' || propietario === '' || fecha === '' || hora === '' || sintomas === '' ) {
            this.setState({
                error: true
            })

            // detener la ejecucion
            return;
        }

        // generar objeto con lso datos
        // asignandole el objeto cita a la constante nuevaCita
        const nuevaCita = {...this.state.cita}
        // creando un nuevo valor para el objeto nuevaCita
        nuevaCita.id = uuid();

        // agregar la cita al state de App
        this.props.crearNuevaCita(nuevaCita);

        // colocar en el state el stateInitial
        this.setState({
          ...stateInitial
        })
    }

    render() {

        // extraer valores del state
        const { error } = this.state;


        return(
            <div className='card mt-5'>
                <div className='card-body'>
                    <h2 className='card-title text-center mb-5'>
                        Llena el formulario para crear una nueva cita
                    </h2>

                    { error ? <div className="alert alert-danger mt-2 mb-5 text-center">Todos los campos son obligatorios</div> : null }

                    <form
                        onSubmit = {this.handleSubmit}
                    >
                        <div className="form-group row">
                            <label htmlFor="" className="col-sm-4 col-lg-2 col-form-label">Nombre Mascota</label>
                            <div className="col-sm-8 col-lg-10">
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre Mascota"
                                    name="mascota"
                                    onChange= {this.handleChage}
                                    value= {this.state.cita.mascota}
                                />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="" className="col-sm-4 col-lg-2 col-form-label">Nombre Dueño</label>
                            <div className="col-sm-8 col-lg-10">
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre Dueño Mascota"
                                    name="propietario"
                                    onChange= {this.handleChage}
                                    value= {this.state.cita.propietario}
                                />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="" className="col-sm-4 col-lg-2 col-form-label">Fecha</label>
                            <div className="col-sm-8 col-lg-4">
                                <input
                                    type="date"
                                    className="form-control"
                                    name="fecha"
                                    onChange= {this.handleChage}
                                    value= {this.state.cita.fecha}
                                />
                            </div>
                            <label htmlFor="" className="col-sm-4 col-lg-2 col-form-label">Hora</label>
                            <div className="col-sm-8 col-lg-4">
                                <input
                                    type="time"
                                    className="form-control"
                                    name="hora"
                                    onChange= {this.handleChage}
                                    value= {this.state.cita.hora}
                                />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="" className="col-sm-4 col-lg-2 col-form-label">Síntomas</label>
                            <div className="col-sm-8 col-lg-10">
                                <textarea
                                    className="form-control"
                                    name="sintomas"
                                    placeholder="Describe los Síntomas"
                                    onChange= {this.handleChage}
                                    value= {this.state.cita.sintomas}
                                ></textarea>
                            </div>

                        </div>
                        <input type="submit" className="py-3 mt-2 btn btn-success btn-block" value="AGREGAR NUEVA CITA"/>
                    </form>
                </div>
            </div>
        );
    }
}

NuevaCita.propTypes = {
    crearNuevaCita: PropTypes.func.isRequired
}
export default NuevaCita;